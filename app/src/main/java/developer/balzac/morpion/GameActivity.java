package developer.balzac.morpion;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.media.MediaPlayer;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import static android.text.TextUtils.isEmpty;

public class GameActivity extends AppCompatActivity implements  View.OnClickListener{
    private static final boolean AUTO_HIDE = true;
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;
    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            // Delayed display of UI elements
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };
    MediaPlayer mp;
    private int idJoueur = 1; //1 = X | 2 = O
    private int scoreX = 0;
    private int scoreO = 0;
    private int etatSoundOff = 0;
    TextView infoJoueur;
    TextView score;
    TextView joueur;
    Button btnSetSoundOff; //on = 0 | off = 1
    Button btnRestartGame;
    Button btnResetScore;
    Button buttontl;
    Button buttontc;
    Button buttontr;
    Button buttoncl;
    Button buttoncc;
    Button buttoncr;
    Button buttonbl;
    Button buttonbc;
    Button buttonbr;
    Dialog dialog;
    EditText namePlayerOne; //joueur X / 1
    EditText namePlayerTwo; //joueur O / 2
    Button btnStart;

    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_game);
        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.joueur);
        infoJoueur = findViewById(R.id.infoJoueur);
        joueur = findViewById(R.id.joueur);
        score = findViewById(R.id.score);
        btnResetScore = findViewById(R.id.btnResetScore);
        btnResetScore.setOnClickListener((View.OnClickListener) this);
        btnRestartGame = findViewById(R.id.btnRestartGame);
        btnRestartGame.setOnClickListener((View.OnClickListener) this);
        btnRestartGame.setClickable(false);
        btnSetSoundOff = findViewById(R.id.btnSoundOff);
        btnSetSoundOff.setOnClickListener((View.OnClickListener) this);
        btnSetSoundOff.setBackgroundResource(R.drawable.ic_volume_on);
        buttontl = findViewById(R.id.buttontl);
        buttontl.setOnClickListener((View.OnClickListener) this);
        buttontc = findViewById(R.id.buttontc);
        buttontc.setOnClickListener((View.OnClickListener) this);
        buttontr = findViewById(R.id.buttontr);
        buttontr.setOnClickListener((View.OnClickListener) this);
        buttoncl = findViewById(R.id.buttoncl);
        buttoncl.setOnClickListener((View.OnClickListener) this);
        buttoncc = findViewById(R.id.buttoncc);
        buttoncc.setOnClickListener((View.OnClickListener) this);
        buttoncr = findViewById(R.id.buttoncr);
        buttoncr.setOnClickListener((View.OnClickListener) this);
        buttonbl = findViewById(R.id.buttonbl);
        buttonbl.setOnClickListener((View.OnClickListener) this);
        buttonbc = findViewById(R.id.buttonbc);
        buttonbc.setOnClickListener((View.OnClickListener) this);
        buttonbr = findViewById(R.id.buttonbr);
        buttonbr.setOnClickListener((View.OnClickListener) this);
        score.setText(scoreX + " - " + scoreO);
        dialog = new Dialog(this);
        startDialog();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnStart:
                joueur.setText(namePlayerOne.getText().toString()+" commence");
                dialog.hide();
                infoJoueur.setText("X ="+namePlayerOne.getText()+"\nO ="+namePlayerTwo.getText());
                break;
            case R.id.btnResetScore:
                resetScore();
                playSoundOnClick();
                startDialog();
                break;
            case R.id.btnRestartGame:
                clearGame();
                playSoundOnClick();
                break;
            case R.id.btnSoundOff:
                if(etatSoundOff == 0) {
                    etatSoundOff ++;
                    btnSetSoundOff.setBackgroundResource(R.drawable.ic_volume_off);
                }
                else if(etatSoundOff == 1){
                    etatSoundOff--;
                    btnSetSoundOff.setBackgroundResource(R.drawable.ic_volume_on);
                }
                break;
            case R.id.buttontl:
                if (idJoueur == 1) {
                    buttontl.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttontl.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttontl.setClickable(false);
                checkWin();
                break;
            case R.id.buttontc:
                if (idJoueur == 1) {
                    buttontc.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttontc.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttontc.setClickable(false);
                checkWin();
                break;
            case R.id.buttontr:
                if (idJoueur == 1) {
                    buttontr.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttontr.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttontr.setClickable(false);
                checkWin();
                break;
            case R.id.buttoncl:
                if (idJoueur == 1) {
                    buttoncl.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttoncl.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttoncl.setClickable(false);
                checkWin();
                break;
            case R.id.buttoncc:
                if (idJoueur == 1) {
                    buttoncc.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttoncc.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttoncc.setClickable(false);
                checkWin();
                break;
            case R.id.buttoncr:
                if (idJoueur == 1) {
                    buttoncr.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttoncr.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttoncr.setClickable(false);
                checkWin();
                break;
            case R.id.buttonbl:
                if (idJoueur == 1) {
                    buttonbl.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttonbl.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttonbl.setClickable(false);
                checkWin();
                break;
            case R.id.buttonbc:
                if (idJoueur == 1) {
                    buttonbc.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttonbc.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttonbc.setClickable(false);
                checkWin();
                break;
            case R.id.buttonbr:
                if (idJoueur == 1) {
                    buttonbr.setText("X");
                    idJoueur++;
                    joueur.setText("Au tour de "+namePlayerTwo.getText().toString());
                } else if (idJoueur == 2) {
                    buttonbr.setText("O");
                    idJoueur--;
                    joueur.setText("Au tour de "+namePlayerOne.getText().toString());
                }
                buttonbr.setClickable(false);
                checkWin();
                break;
        }
    }

    public void checkWin() {
        if (buttontl.getText() == buttontc.getText() && buttontc.getText() == buttontr.getText() && !isEmpty(buttontc.getText())) {
            showWinner();
            btnRestartGame.setClickable(true);
            setScore();
            lockBtn();
            playSoundOnWin();
        } else if (buttoncl.getText() == buttoncc.getText() && buttoncc.getText() == buttoncr.getText() && !isEmpty(buttoncc.getText())) {
            showWinner();
            btnRestartGame.setClickable(true);
            setScore();
            lockBtn();
            playSoundOnWin();
        } else if (buttonbl.getText() == buttonbc.getText() && buttonbc.getText() == buttonbr.getText() && !isEmpty(buttonbc.getText())) {
            showWinner();
            btnRestartGame.setClickable(true);
            setScore();
            lockBtn();
            playSoundOnWin();
        } else if (buttontl.getText() == buttoncl.getText() && buttoncl.getText() == buttonbl.getText() && !isEmpty(buttoncl.getText())) {
            showWinner();
            btnRestartGame.setClickable(true);
            setScore();
            lockBtn();
            playSoundOnWin();
        } else if (buttontc.getText() == buttoncc.getText() && buttoncc.getText() == buttonbc.getText() && !isEmpty(buttoncc.getText())) {
            showWinner();
            btnRestartGame.setClickable(true);
            setScore();
            lockBtn();
            playSoundOnWin();
        } else if (buttontr.getText() == buttoncr.getText() && buttoncr.getText() == buttonbr.getText() && !isEmpty(buttoncr.getText())) {
            showWinner();
            btnRestartGame.setClickable(true);
            setScore();
            lockBtn();
            playSoundOnWin();
        } else if (buttontl.getText() == buttoncc.getText() && buttoncc.getText() == buttonbr.getText() && !isEmpty(buttoncc.getText())) {
            showWinner();
            btnRestartGame.setClickable(true);
            setScore();
            lockBtn();
            playSoundOnWin();
        } else if (buttontr.getText() == buttoncc.getText() && buttoncc.getText() == buttonbl.getText() && !isEmpty(buttoncc.getText())) {
            showWinner();
            btnRestartGame.setClickable(true);
            setScore();
            lockBtn();
            playSoundOnWin();
        } else if (!isEmpty(buttontl.getText()) && !isEmpty(buttontc.getText()) && !isEmpty(buttontr.getText()) && !isEmpty(buttoncl.getText()) &&
                !isEmpty(buttoncc.getText()) && !isEmpty(buttoncr.getText()) && !isEmpty(buttonbl.getText()) && !isEmpty(buttonbc.getText()) &&
                !isEmpty(buttonbr.getText())) {
            btnRestartGame.setClickable(true);
            joueur.setText("Egalité !");
            lockBtn();
        }
        else {
            playSoundOnClick();
            btnRestartGame.setClickable(false);
        }
    }

    private void showWinner() {
        if(idJoueur == 1){
            joueur.setText(namePlayerTwo.getText()+" gagne cette manche !");
        }
        else if(idJoueur == 2){
            joueur.setText(namePlayerOne.getText()+" gagne cette manche !");
        }
    }

    private void setScore() {
        if (idJoueur == 1) {
            scoreO++;
        } else if (idJoueur == 2) {
            scoreX++;
        }
        score.setText(scoreX + " - " + scoreO);
        if (scoreO == 3| scoreX == 3){
            if (idJoueur == 1) {
                score.setText(namePlayerTwo.getText()+" gagne la partie !");
            } else if (idJoueur == 2) {
                score.setText(namePlayerOne.getText()+" gagne la partie !");
            }
            btnRestartGame.setClickable(false);
            joueur.setText("Appuyer sur rejouer pour relancer une partie");
        }
    }

    private void lockBtn() {
        buttontl.setClickable(false);
        buttontc.setClickable(false);
        buttontr.setClickable(false);
        buttoncl.setClickable(false);
        buttoncc.setClickable(false);
        buttoncr.setClickable(false);
        buttonbl.setClickable(false);
        buttonbc.setClickable(false);
        buttonbr.setClickable(false);
    }

    private void unlockBtn() {
        buttontl.setClickable(true);
        buttontc.setClickable(true);
        buttontr.setClickable(true);
        buttoncl.setClickable(true);
        buttoncc.setClickable(true);
        buttoncr.setClickable(true);
        buttonbl.setClickable(true);
        buttonbc.setClickable(true);
        buttonbr.setClickable(true);
    }

    public void clearGame() {
        buttontl.setText("");
        buttontc.setText("");
        buttontr.setText("");
        buttoncl.setText("");
        buttoncc.setText("");
        buttoncr.setText("");
        buttonbl.setText("");
        buttonbc.setText("");
        buttonbr.setText("");
        idJoueur = 1;
        unlockBtn();
        joueur.setText("le joueur "+namePlayerOne.getText()+" commence");
    }

    private void resetScore() {
        scoreX = 0;
        scoreO = 0;
        score.setText(scoreX + " - " + scoreO);
        clearGame();
    }

    protected void playSoundOnClick() {
        if (getEtatSound()) {
            if (mp != null) {
                mp.reset();
                mp.release();
            }
            mp = MediaPlayer.create(this, R.raw.click);
            mp.start();
        }
    }

    protected void playSoundOnWin() {
        if(getEtatSound()) {
            if (mp != null) {
                mp.reset();
                mp.release();
            }
            mp = MediaPlayer.create(this, R.raw.win);
            mp.start();
        }
    }

    protected boolean getEtatSound() {
        if (etatSoundOff == 0) {
            return true;
        }
        return false;
    }

    public void startDialog() {
        dialog.setContentView(R.layout.dialog_start);
        dialog.setTitle(R.string.app_name);
        dialog.show();
        namePlayerOne = dialog.findViewById(R.id.editTextName1);
        namePlayerTwo = dialog.findViewById(R.id.editTextName2);
        btnStart = dialog.findViewById(R.id.btnStart);
        btnStart.setOnClickListener((View.OnClickListener) this);
    }



    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }


    private void hide() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }
}
